{{- define "labels" -}}
app.kubernetes.io/managed-by: {{ $.Release.Service }}
helm.sh/chart: {{ $.Chart.Name }}
{{ include "selectorLabels" . }}
{{- end -}}

{{- define "selectorLabels" -}}
app.kubernetes.io/component: ark
app.kubernetes.io/instance: {{ $.Release.Name }}
{{- end -}}


{{- define "modIds" -}}
{{ join "," .Values.config.mods | quote }}
{{- end -}}
