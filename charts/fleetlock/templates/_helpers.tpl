{{- define "fleetlock.labels" -}}
{{ include "fleetlock.selectorLabels" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/component: fleetlock
{{- end -}}

{{- define "fleetlock.selectorLabels" -}}
app.kubernetes.io/name: fleetlock
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}
