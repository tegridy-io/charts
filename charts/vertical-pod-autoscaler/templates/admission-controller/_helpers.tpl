{{- define "vertical-pod-autoscaler.admission-controller.labels" -}}
{{ include "vertical-pod-autoscaler.labels" . }}
app.kubernetes.io/component: admission-controller
{{- end -}}

{{- define "vertical-pod-autoscaler.admission-controller.selectorLabels" -}}
{{ include "vertical-pod-autoscaler.selectorLabels" . }}
app.kubernetes.io/component: admission-controller
{{- end -}}
