{{- define "vertical-pod-autoscaler.labels" -}}
{{ include "vertical-pod-autoscaler.selectorLabels" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{- define "vertical-pod-autoscaler.selectorLabels" -}}
app.kubernetes.io/name: vertical-pod-autoscaler
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}
