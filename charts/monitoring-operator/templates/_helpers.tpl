{{- define "labels" -}}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart: {{ .Chart.Name }}
{{- end -}}



{{- define "operatorName" -}}
{{- if .Values.operator.nameOverride -}}
{{ .Values.operator.nameOverride }}
{{- else if .Values.operator.namePrefix -}}
{{ printf "%s-%s" .Values.operator.namePrefix "prometheus-operator" }}
{{- else -}}
prometheus-operator
{{- end -}}
{{- end -}}

{{- define "operatorLabels" -}}
{{ include "operatorSelectorLabels" . }}
{{ include "labels" . }}
{{- end -}}

{{- define "operatorSelectorLabels" -}}
app.kubernetes.io/name: {{ include "operatorName" . }}
app.kubernetes.io/component: operator
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}
