{{- define "suc.labels" -}}
{{ include "suc.selectorLabels" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/component: system-upgrade-controller
{{- end -}}

{{- define "suc.selectorLabels" -}}
app.kubernetes.io/name: system-upgrade-controller
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}
