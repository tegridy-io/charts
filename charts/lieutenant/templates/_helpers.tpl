{{- define "labels" -}}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart: {{ .Chart.Name }}
{{- end -}}



{{- define "operatorName" -}}
{{- if .Values.operator.nameOverride -}}
{{ .Values.operator.nameOverride }}
{{- else -}}
lieutenant-operator-controller-manager
{{- end -}}
{{- end -}}

{{- define "operatorNameShort" -}}
{{- if .Values.operator.nameOverride -}}
{{ .Values.operator.nameOverride }}
{{- else -}}
lieutenant-operator-manager
{{- end -}}
{{- end -}}

{{- define "operatorNameSA" -}}
{{- if .Values.operator.nameOverride -}}
{{ .Values.operator.nameOverride }}
{{- else -}}
lieutenant-operator
{{- end -}}
{{- end -}}

{{- define "operatorLabels" -}}
{{ include "operatorSelectorLabels" . }}
{{ include "labels" . }}
{{- end -}}

{{- define "operatorSelectorLabels" -}}
app.kubernetes.io/component: operator
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}



{{- define "apiName" -}}
{{- if .Values.api.nameOverride -}}
{{ .Values.api.nameOverride }}
{{- else -}}
lieutenant-api
{{- end -}}
{{- end -}}

{{- define "apiLabels" -}}
{{ include "apiSelectorLabels" . }}
{{ include "labels" . }}
{{- end -}}

{{- define "apiSelectorLabels" -}}
app.kubernetes.io/component: api
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}
